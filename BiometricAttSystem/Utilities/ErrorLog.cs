﻿using System;
using System.Text;
using System.IO;

namespace Seeds_Billing_App_Utilities
{
    public class ErrorLog
    {
        public static string CreateErrorMessage(Exception serviceException)
        {
            var messageBuilder = new StringBuilder();
            try
            {
                messageBuilder.Append("The Exception is:-");

                messageBuilder.Append("Exception :: " + serviceException.ToString());
                if (serviceException.InnerException != null)
                {

                    messageBuilder.Append("InnerException :: " + serviceException.InnerException.ToString());
                }
                return messageBuilder.ToString();
            }
            catch
            {
                messageBuilder.Append("Exception:: Unknown Exception.");
                return messageBuilder.ToString();
            }

        }

        public static void LogFileWrite(string message)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                var logFilePath = "c:\\BiometricApplicationError\\";

                logFilePath = logFilePath + "ProgramLog" + "-" + DateTime.Today.ToString("yyyyMMdd") + "." + "txt";

                if (logFilePath.Equals("")) return;
                #region Create the Log file directory if it does not exists
                DirectoryInfo logDirInfo = null;
                var logFileInfo = new FileInfo(logFilePath);
                if (logFileInfo.DirectoryName != null) logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (logDirInfo != null && !logDirInfo.Exists) logDirInfo.Create();
                #endregion Create the Log file directory if it does not exists

                fileStream = !logFileInfo.Exists ? logFileInfo.Create() : new FileStream(logFilePath, FileMode.Append);
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(message);
            }
            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }
    }
}

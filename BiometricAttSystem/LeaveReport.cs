﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BiometricAttSystem.Entities;
using BiometricAttSystem.DataLayer;

namespace BiometricAttSystem
{
    public partial class LeaveReport : Form
    {
        public LeaveReport()
        {
            InitializeComponent();
        }

        private void cmbEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string month = "0";
            switch (cmbEmp.SelectedItem.ToString())
            {
                case "JANUARY":
                    month= "01";
                    break;
                case "FEBRUARY":
                    month = "02";
                    break;
                case "MARCH":
                    month = "03";
                    break;
                case "APRIL":
                    month = "04";
                    break;
                case "MAY":
                    month = "05";
                    break;
                case "JUNE":
                    month = "06";
                    break;
                case "JULY":
                    month = "07";
                    break;
                case "AUGUST":
                    month = "08";
                    break;
                case "SEPTEMBER":
                    month = "09";
                    break;
                case "OCTOBER":
                    month = "10";
                    break;
                case "NOVEMBER":
                    month = "11";
                    break;
                case "DECEMBER":
                    month = "12";
                    break;
            }
            FormDataTableBasedOnInputValues(month);

        }

        private DateTime[] GetNoOfDaysinSelectedMonth(string selectedMonth)
        {
            try
            {
                var date = selectedMonth + "/01/" + DateTime.Now.Year;
                var dt = DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                int numberOfDays = DateTime.DaysInMonth(dt.Year, dt.Month);
                var endDate = DateTime.ParseExact(selectedMonth + "/" + numberOfDays.ToString() + "/" + DateTime.Now.Year, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                return Enumerable.Range(0, 1 + endDate.Subtract(dt).Days)
                .Select(offset => dt.AddDays(offset))
                .ToArray();
            }

            catch
            {
                throw;
            }
           
        }

        public List<Entities.Leave> GetLeaveData(string selectedMonth)
        {
            try
            {
                var reports = new DataLayer.Reports();
                return reports.GetLeaveReport(Convert.ToInt32(selectedMonth));
            }
            catch
            {
                throw;
            }
           
        }

        public List<Employee> GetEmployeeInformation()
        {
            try
            {
                var attendance = new DataLayer.Attendance();
                return  attendance.GetEmployeeEnrolmentandFingerDetails();
                
            }
            catch
            {
                throw;
            }
            
        }
        public DataTable FormDataTableBasedOnInputValues(string selectedMonth)
        {
            try
            {
                var leaveData = GetLeaveData(selectedMonth);
                var dt = new DataTable();
                dt.Columns.Add("Employee Name");
                dt.Columns.Add("Department");
                dt.Columns.Add("Designation");
                dt.Columns.Add("Leave Type");
                dt.Columns.Add("No of Days");
                dt.Columns.Add("Date From");
                dt.Columns.Add("Date To");

                foreach (var leave in leaveData)
                {
                    dt.Rows.Add(leave.EmployeeFName, leave.Department, leave.Designation, leave.TypeOfLeave, leave.NoofDays, DateTime.Parse(leave.leavefromdate).ToShortDateString(), DateTime.Parse(leave.leaveToDate).ToShortDateString());
                }

                dataGridView1.DataSource = dt;
            }

            catch
            {
                throw;
            }
           
            return null;
        }

        private void btnEnterLeave_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            var CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

        private void copyAlltoClipboard()
        {
            dataGridView1.SelectAll();
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }
    }
}

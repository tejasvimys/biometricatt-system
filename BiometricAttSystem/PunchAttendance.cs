﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Seeds_Billing_App_Utilities;
using BiometricAttSystem.DataLayer;
using SecuGen.FDxSDKPro.Windows;
using System.Threading;
using System.IO;

namespace BiometricAttSystem
{
    public partial class PunchAttendance : Form
    {
        private Int32 m_ImageWidth;
        private Int32 m_ImageHeight;
        private Byte[] m_RegMin1;
        private Byte[] m_RegMin2;
        private Byte[] m_RegMin3;
        private Byte[] m_RegMin4;
        private Byte[] m_VrfMin;
        private string EnrolmentId;

        public SGFingerPrintManager m_FPM;
        private SGFPMDeviceList[] m_DevList; // Used for EnumerateDevice
        public PunchAttendance()
        {
            InitializeComponent();
            button1.Enabled = false;
        }

        //ADDING ATTENDANCE
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var empId = txtEnrolmentNo.Text.Trim();
                if (string.IsNullOrEmpty(empId))
                    return;
                EnrolmentId = empId;
                GetEmployeeInformation(empId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private void t_Tick(object sender, EventArgs e)
        {
            try
            {
                //get current time
                int hh = DateTime.Now.Hour;
                int mm = DateTime.Now.Minute;
                int ss = DateTime.Now.Second;

                //time
                string time = "";

                //padding leading zero
                if (hh < 10)
                {
                    time += "0" + hh;
                }
                else
                {
                    time += hh;
                }
                time += ":";

                if (mm < 10)
                {
                    time += "0" + mm;
                }
                else
                {
                    time += mm;
                }
                time += ":";

                if (ss < 10)
                {
                    time += "0" + ss;
                }
                else
                {
                    time += ss;
                }

                //update label
                lblTime.Text = time;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }

        }

        private void PunchAttendance_Load(object sender, EventArgs e)
        {
            try
            {
                var dateTime = DateTime.UtcNow.Date;
                lblDate.Text = dateTime.ToString("dd-MM-yyyy");
                t.Interval = 1000;  //in milliseconds

                t.Tick += new EventHandler(this.t_Tick);

                //start timer when form loads
                t.Start();  //this will use t_Tick() method

                m_RegMin1 = new Byte[400];
                m_RegMin2 = new Byte[400];
                m_RegMin3 = new Byte[400];
                m_RegMin4 = new Byte[400];
                m_VrfMin = new Byte[400];

                comboBoxSecuLevel_R.SelectedIndex = 4;
                comboBoxSecuLevel_V.SelectedIndex = 3;

                m_FPM = new SGFingerPrintManager();

                Int32 iError;
                string enum_device;

                comboBoxDeviceName.Items.Clear();

                // Enumerate Device
                iError = m_FPM.EnumerateDevice();

                // Get enumeration info into SGFPMDeviceList
                m_DevList = new SGFPMDeviceList[m_FPM.NumberOfDevice];

                for (int i = 0; i < m_FPM.NumberOfDevice; i++)
                {
                    m_DevList[i] = new SGFPMDeviceList();
                    m_FPM.GetEnumDeviceInfo(i, m_DevList[i]);
                    enum_device = m_DevList[i].DevName.ToString() + " : " + m_DevList[i].DevID;
                    comboBoxDeviceName.Items.Add(enum_device);
                }

                if (comboBoxDeviceName.Items.Count > 0)
                {
                    // Add Auto Selection
                    enum_device = "Auto Selection";
                    comboBoxDeviceName.Items.Add(enum_device);

                    comboBoxDeviceName.SelectedIndex = 0;  //First selected one
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private void GetEmployeeInformation(string employeeenrolmentid)
        {
            try
            {
                Int32 iError;
                var att = new Attendance();
                var attDetails = att.GetEmployeeDetails(employeeenrolmentid);

                if(attDetails.f1t1!=null || attDetails.f1t2 != null || attDetails.f2t1 != null || attDetails.f2t2 != null)
                {
                    iError = m_FPM.CreateTemplate(attDetails.f1t1, m_RegMin1);
                    iError = m_FPM.CreateTemplate(attDetails.f1t2, m_RegMin2);
                    iError = m_FPM.CreateTemplate(attDetails.f2t1, m_RegMin3);
                    iError = m_FPM.CreateTemplate(attDetails.f2t2, m_RegMin4);

                    Thread.Sleep(5000);                   
                    CaptureFpForVerification();
                    //lblFirstName.Text = attDetails.EmployeeFName;
                    //lblLastName.Text = attDetails.EmployeeLName;
                    //lblDateIn.Text = DateTime.Now.ToLongDateString();
                    //lblTimeIn.Text = DateTime.Now.ToLongTimeString();
                    //pbEmployeePhoto.Image = byteArrayToImage(attDetails.empPic);
                    var dataverification = new DataVerification(attDetails.EmployeeFName, attDetails.EmployeeLName, DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), attDetails.empPic);
                    dataverification.ShowDialog(this);
                    dataverification.Dispose();
                }
                else
                {
                    MessageBox.Show("Employee Details Not Found! Enrol Employee to continue to Attendance");
                }
                //Thread.Sleep(10000);
                ClearFields();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private void OpenDeviceBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_FPM.NumberOfDevice == 0)
                    return;

                SGFPMDeviceName device_name;
                Int32 device_id;

                Int32 numberOfDevices = comboBoxDeviceName.Items.Count;
                Int32 deviceSelected = comboBoxDeviceName.SelectedIndex;
                Boolean autoSelection = (deviceSelected == (numberOfDevices - 1));  // Last index

                if (autoSelection)
                {
                    device_name = SGFPMDeviceName.DEV_AUTO;
                    device_id = (Int32)(SGFPMPortAddr.USB_AUTO_DETECT);
                }
                else
                {
                    device_name = m_DevList[deviceSelected].DevName;
                    device_id = m_DevList[deviceSelected].DevID;
                }

                Int32 iError = OpenDevice(device_name, device_id);
                button1.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private Int32 OpenDevice(SGFPMDeviceName device_name, Int32 device_id)
        {
            try
            {
                Int32 iError = m_FPM.Init(device_name);
                iError = m_FPM.OpenDevice(device_id);

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    GetBtn_Click(null, null);
                    lblNotification.Text = "Initialization Success";

                }
                else
                    DisplayError("OpenDevice()", iError);
                return iError;
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
                return 1000;
            }
        }

        private void CloseDevice()
        {
            m_FPM.CloseDevice();
        }

        void DisplayError(string funcName, int iError)
        {
            string text = "";

            switch (iError)
            {
                case 0:                             //SGFDX_ERROR_NONE				= 0,
                    text = "Error none";
                    break;

                case 1:                             //SGFDX_ERROR_CREATION_FAILED	= 1,
                    text = "Can not create object";
                    break;

                case 2:                             //   SGFDX_ERROR_FUNCTION_FAILED	= 2,
                    text = "Function Failed";
                    break;

                case 3:                             //   SGFDX_ERROR_INVALID_PARAM	= 3,
                    text = "Invalid Parameter";
                    break;

                case 4:                          //   SGFDX_ERROR_NOT_USED			= 4,
                    text = "Not used function";
                    break;

                case 5:                                //SGFDX_ERROR_DLLLOAD_FAILED	= 5,
                    text = "Can not create object";
                    break;

                case 6:                                //SGFDX_ERROR_DLLLOAD_FAILED_DRV	= 6,
                    text = "Can not load device driver";
                    break;
                case 7:                                //SGFDX_ERROR_DLLLOAD_FAILED_ALGO = 7,
                    text = "Can not load sgfpamx.dll";
                    break;

                case 51:                //SGFDX_ERROR_SYSLOAD_FAILED	   = 51,	// system file load fail
                    text = "Can not load driver kernel file";
                    break;

                case 52:                //SGFDX_ERROR_INITIALIZE_FAILED  = 52,   // chip initialize fail
                    text = "Failed to initialize the device";
                    break;

                case 53:                //SGFDX_ERROR_LINE_DROPPED		   = 53,   // image data drop
                    text = "Data transmission is not good";
                    break;

                case 54:                //SGFDX_ERROR_TIME_OUT			   = 54,   // getliveimage timeout error
                    text = "Time out";
                    break;

                case 55:                //SGFDX_ERROR_DEVICE_NOT_FOUND	= 55,   // device not found
                    text = "Device not found";
                    break;

                case 56:                //SGFDX_ERROR_DRVLOAD_FAILED	   = 56,   // dll file load fail
                    text = "Can not load driver file";
                    break;

                case 57:                //SGFDX_ERROR_WRONG_IMAGE		   = 57,   // wrong image
                    text = "Wrong Image";
                    break;

                case 58:                //SGFDX_ERROR_LACK_OF_BANDWIDTH  = 58,   // USB Bandwith Lack Error
                    text = "Lack of USB Bandwith";
                    break;

                case 59:                //SGFDX_ERROR_DEV_ALREADY_OPEN	= 59,   // Device Exclusive access Error
                    text = "Device is already opened";
                    break;

                case 60:                //SGFDX_ERROR_GETSN_FAILED		   = 60,   // Fail to get Device Serial Number
                    text = "Device serial number error";
                    break;

                case 61:                //SGFDX_ERROR_UNSUPPORTED_DEV		   = 61,   // Unsupported device
                    text = "Unsupported device";
                    break;

                // Extract & Verification error
                case 101:                //SGFDX_ERROR_FEAT_NUMBER		= 101, // utoo small number of minutiae
                    text = "The number of minutiae is too small";
                    break;

                case 102:                //SGFDX_ERROR_INVALID_TEMPLATE_TYPE		= 102, // wrong template type
                    text = "Template is invalid";
                    break;

                case 103:                //SGFDX_ERROR_INVALID_TEMPLATE1		= 103, // wrong template type
                    text = "1st template is invalid";
                    break;

                case 104:                //SGFDX_ERROR_INVALID_TEMPLATE2		= 104, // vwrong template type
                    text = "2nd template is invalid";
                    break;

                case 105:                //SGFDX_ERROR_EXTRACT_FAIL		= 105, // extraction fail
                    text = "Minutiae extraction failed";
                    break;

                case 106:                //SGFDX_ERROR_MATCH_FAIL		= 106, // matching  fail
                    text = "Matching failed";
                    break;

            }

            text = funcName + " Error # " + iError + " :" + text;
            lblNotification.Text = text;
        }

        private void CaptureFpForVerification()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Int32 iError;
                Byte[] fp_image;
                Int32 img_qlty;

                fp_image = new Byte[m_ImageWidth * m_ImageHeight];
                img_qlty = 0;

                iError = m_FPM.GetImage(fp_image);

                m_FPM.GetImageQuality(m_ImageWidth, m_ImageHeight, fp_image, ref img_qlty);
                pgEmpTemplate.Value = img_qlty;

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    DrawImage(fp_image, pbEmpFingerTemplate);
                    iError = m_FPM.CreateTemplate(null, fp_image, m_VrfMin);

                    if (iError == (Int32)SGFPMError.ERROR_NONE)
                    {
                        ValidateFingerTemplates();
                    }
                       
                    else
                        DisplayError("CreateTemplate()", iError);
                }
                else
                    DisplayError("GetImage()", iError);

                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetBtn_Click(object sender, System.EventArgs e)
        {
            SGFPMDeviceInfoParam pInfo = new SGFPMDeviceInfoParam();
            Int32 iError = m_FPM.GetDeviceInfo(pInfo);

            if (iError == (Int32)SGFPMError.ERROR_NONE)
            {
                m_ImageWidth = pInfo.ImageWidth;
                m_ImageHeight = pInfo.ImageHeight;
            }
        }

        public void DrawImage(Byte[] imgData, PictureBox picBox)
        {
            int colorval;
            Bitmap bmp = new Bitmap(m_ImageWidth, m_ImageHeight);
            picBox.Image = (Image)bmp;

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    colorval = (int)imgData[(j * m_ImageWidth) + i];
                    bmp.SetPixel(i, j, Color.FromArgb(colorval, colorval, colorval));
                }
            }
            picBox.Refresh();
        }

        private void ValidateFingerTemplates()
        {
            try
            {
                Int32 iError;
                bool matched1 = false;
                bool matched2 = false;
                bool matched3 = false;
                bool matched4 = false;
                SGFPMSecurityLevel secu_level;

                secu_level = (SGFPMSecurityLevel)comboBoxSecuLevel_V.SelectedIndex;

                iError = m_FPM.MatchTemplate(m_RegMin1, m_VrfMin, secu_level, ref matched1);
                iError = m_FPM.MatchTemplate(m_RegMin2, m_VrfMin, secu_level, ref matched2);
                iError = m_FPM.MatchTemplate(m_RegMin3, m_VrfMin, secu_level, ref matched3);
                iError = m_FPM.MatchTemplate(m_RegMin4, m_VrfMin, secu_level, ref matched4);

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    if (matched1 || matched2 || matched3|| matched4)
                    {  
                        var att = new Attendance();
                        if (!GetEmployeeMarkedOut())
                        {
                            EnterAttendance();
                        }
                        else
                        {
                            UpdateAttendance();
                        }
                        //MessageBox.Show("Verification Success! Attendace marked in for the day!");
                    }

                    else
                    {
                        MessageBox.Show("Verification Failed");                     
                    }

                }
                else
                    DisplayError("MatchTemplate()", iError);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private void EnterAttendance()
        {
            try
            {
                var att = new Attendance();
                att.EnterAttendance(EnrolmentId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private void UpdateAttendance()
        {
            try
            {
                var att = new Attendance();
                att.UpdateAttendance(EnrolmentId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        private bool GetEmployeeMarkedOut()
        {
            try
            {
                var att = new Attendance();
                return att.GetIfEmployeePunchedAttendance(EnrolmentId);
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
        }

        private void ClearFields()
        {
            try
            {
                lblFirstName.Text = string.Empty;
                lblLastName.Text = string.Empty;
                lblDateIn.Text = string.Empty;
                lblTimeIn.Text = string.Empty;
                pbEmpFingerTemplate.Image = null;
                pbEmployeePhoto.Image = null;
                pgEmpTemplate.Value = 0;
                txtEnrolmentNo.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error ==========>" + ex);
                ErrorLog.LogFileWrite(ex.ToString());
            }
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}


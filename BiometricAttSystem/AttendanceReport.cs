﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BiometricAttSystem.Entities;
using BiometricAttSystem.DataLayer;

namespace BiometricAttSystem
{
    public partial class AttendanceReport : Form
    {
        public AttendanceReport()
        {
            InitializeComponent();
        }

        private void cmbEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string month = "0";
            switch (cmbEmp.SelectedItem.ToString())
            {
                case "JANUARY":
                    month= "01";
                    break;
                case "FEBRUARY":
                    month = "02";
                    break;
                case "MARCH":
                    month = "03";
                    break;
                case "APRIL":
                    month = "04";
                    break;
                case "MAY":
                    month = "05";
                    break;
                case "JUNE":
                    month = "06";
                    break;
                case "JULY":
                    month = "07";
                    break;
                case "AUGUST":
                    month = "08";
                    break;
                case "SEPTEMBER":
                    month = "09";
                    break;
                case "OCTOBER":
                    month = "10";
                    break;
                case "NOVEMBER":
                    month = "11";
                    break;
                case "DECEMBER":
                    month = "12";
                    break;
            }
            FormDataTableBasedOnInputValues(month);

        }

        private DateTime[] GetNoOfDaysinSelectedMonth(string selectedMonth)
        {
            try
            {
                var date = selectedMonth + "/01/" + DateTime.Now.Year;
                var dt = DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                int numberOfDays = DateTime.DaysInMonth(dt.Year, dt.Month);
                var endDate = DateTime.ParseExact(selectedMonth + "/" + numberOfDays.ToString() + "/" + DateTime.Now.Year, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                return Enumerable.Range(0, 1 + endDate.Subtract(dt).Days)
                .Select(offset => dt.AddDays(offset))
                .ToArray();
            }

            catch
            {
                throw;
            }
           
        }

        public List<Entities.Attendance> GetAttendanceData(string selectedMonth)
        {
            try
            {
                var reports = new DataLayer.Reports();
                return reports.GetAttendanceReport(Convert.ToInt32(selectedMonth));
            }
            catch
            {
                throw;
            }
           
        }

        public List<Employee> GetEmployeeInformation()
        {
            try
            {
                var attendance = new DataLayer.Attendance();
                return  attendance.GetEmployeeEnrolmentandFingerDetails();
                
            }
            catch
            {
                throw;
            }
            
        }

        public List<Entities.Leave> GetLeaveData(string selectedMonth)
        {
            try
            {
                var reports = new DataLayer.Reports();
                return reports.GetLeaveReport(Convert.ToInt32(selectedMonth));
            }
            catch
            {
                throw;
            }

        }
        public DataTable FormDataTableBasedOnInputValues(string selectedMonth)
        {
            try
            {
                var employee = GetEmployeeInformation();
                var attendance = GetAttendanceData(selectedMonth);
                var leave = GetLeaveData(selectedMonth);

                var dt = new DataTable();   
                var noofDaysinamonth = GetNoOfDaysinSelectedMonth(selectedMonth);

                dt.Columns.Add("Employee Enrolment Id");
                dt.Columns.Add("First Name");
                dt.Columns.Add("Department");
                dt.Columns.Add("Designation");

                foreach (var days in noofDaysinamonth)
                {
                    dt.Columns.Add(days.ToShortDateString());
                }
                


                foreach (var rowObj in employee)
                {                  
                    var val = attendance.FindAll(x => x.EmployeeEnrolmentId == rowObj.EmployeeId.ToString());
                    var attleaves = leave.FindAll((x => x.EmployeeId == rowObj.EmployeeId.ToString()));
                    List<Entities.Leave> dates = null;
                    if(attleaves.Count>0)
                    {
                        dates = GetDatesBetween(attleaves);
                    }

                    var dr = dt.NewRow();
                    dr[0] = rowObj.EnrolmentId;
                    dr[1] = rowObj.EmployeeFName;
                    dr[2] = rowObj.Department;
                    dr[3] = rowObj.Designation;
                    if(dates!=null && dates.Count > 0)
                    {
                        foreach (var leaves in dates)
                        {
                            dr[DateTime.Parse(leaves.leavefromdate).ToShortDateString()] = leaves.TypeOfLeave;
                        }
                    }
                  
                    foreach(var value in val)
                    {
                        var timein = value.TimeIn != string.Empty ? DateTime.Parse(value.TimeIn).ToShortTimeString():"";
                        var timeout = value.TimeOut != string.Empty ? DateTime.Parse(value.TimeOut).ToShortTimeString() : "";

                        dr[DateTime.Parse(value.DateIn).ToShortDateString()] = "IN TIME:- " + timein + " ------- OUT TIME: " + timeout;
                        
                        
                    }
                    dt.Rows.Add(dr);
                    //dt.Rows.Add(rowObj.EnrolmentId, rowObj.EmployeeFName, rowObj.Department, rowObj.Designation, "","","");
                }
                dataGridView1.DataSource = dt;
            }

            catch
            {
                throw;
            }
           
            return null;
        }

        public List<Entities.Leave> GetDatesBetween(List<Entities.Leave> leavesApplied)
        {
            var allDates = new List<Entities.Leave>();
            foreach (var leave in leavesApplied)
            {  
                for (DateTime date = Convert.ToDateTime(leave.leavefromdate); date <= Convert.ToDateTime(leave.leaveToDate); date = date.AddDays(1))
                {
                    var leaveDetails = new Entities.Leave();
                    leaveDetails.leavefromdate = (date).ToShortDateString();
                    leaveDetails.TypeOfLeave = leave.TypeOfLeave;
                    allDates.Add(leaveDetails);
                }
            }
            return allDates;
        }

        private void btnEnterLeave_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
           var CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

        private void copyAlltoClipboard()
        {
            dataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridView1.SelectAll();     
            DataObject dataObj = dataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }
    }
}

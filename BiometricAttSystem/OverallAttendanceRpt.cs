﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BiometricAttSystem.Entities;
using BiometricAttSystem.DataLayer;

namespace BiometricAttSystem
{
    public partial class OverallAttendanceRpt : Form
    {
        public OverallAttendanceRpt()
        {
            InitializeComponent();
        }

        private void cmbEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            string month = "0";
            switch (cmbEmp.SelectedItem.ToString())
            {
                case "JANUARY":
                    month= "01";
                    break;
                case "FEBRUARY":
                    month = "02";
                    break;
                case "MARCH":
                    month = "03";
                    break;
                case "APRIL":
                    month = "04";
                    break;
                case "MAY":
                    month = "05";
                    break;
                case "JUNE":
                    month = "06";
                    break;
                case "JULY":
                    month = "07";
                    break;
                case "AUGUST":
                    month = "08";
                    break;
                case "SEPTEMBER":
                    month = "09";
                    break;
                case "OCTOBER":
                    month = "10";
                    break;
                case "NOVEMBER":
                    month = "11";
                    break;
                case "DECEMBER":
                    month = "12";
                    break;
            }
            FormDataTableBasedOnInputValues(month);

        }

        private DateTime[] GetNoOfDaysinSelectedMonth(string selectedMonth)
        {
            try
            {
                var date = selectedMonth + "/01/" + DateTime.Now.Year;
                var dt = DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                int numberOfDays = DateTime.DaysInMonth(dt.Year, dt.Month);
                var endDate = DateTime.ParseExact(selectedMonth + "/" + numberOfDays.ToString() + "/" + DateTime.Now.Year, "MM/dd/yyyy", CultureInfo.InvariantCulture);

                return Enumerable.Range(0, 1 + endDate.Subtract(dt).Days)
                .Select(offset => dt.AddDays(offset))
                .ToArray();
            }

            catch
            {
                throw;
            }
           
        }

        public List<Entities.Attendance> GetOverallAttendance(string selectedMonth)
        {
            try
            {
                var reports = new DataLayer.Reports();
                return reports.GetAttendanceTotalReport(Convert.ToInt32(selectedMonth));
            }
            catch
            {
                throw;
            }
           
        }

        public List<Employee> GetEmployeeInformation()
        {
            try
            {
                var attendance = new DataLayer.Attendance();
                return  attendance.GetEmployeeEnrolmentandFingerDetails();
                
            }
            catch
            {
                throw;
            }
            
        }
        public DataTable FormDataTableBasedOnInputValues(string selectedMonth)
        {
            try
            {
                var employee = GetEmployeeInformation();
                var attendance = GetOverallAttendance(selectedMonth);
                var dt = new DataTable();

                var query = from person in employee
                            join pet in attendance on person.EmployeeId equals pet.EmployeeEnrolmentId into gj
                            from subpet in gj.DefaultIfEmpty()
                            select new { EnrolmentId = person.EnrolmentId, FirstName = person.EmployeeFName, Dept = person.Department, Desig = person.Designation, Att = subpet?.DaysAttended ?? 0 };


                dt.Columns.Add("Employee Enrolment Id");
                dt.Columns.Add("First Name");
                dt.Columns.Add("Department");
                dt.Columns.Add("Designation");
                dt.Columns.Add("No of Days Attended");
                DataRow row = null;
                foreach (var rowObj in query)
                {
                    row = dt.NewRow();
                    dt.Rows.Add(rowObj.EnrolmentId, rowObj.FirstName, rowObj.Dept, rowObj.Desig, rowObj.Att);
                }
                dgAttRpt.DataSource = dt;

            }

            catch
            {
                throw;
            }
           
            return null;
        }

        private void btnEnterLeave_Click(object sender, EventArgs e)
        {
           
        }

        private void copyAlltoClipboard()
        {
            dgAttRpt.SelectAll();
            DataObject dataObj = dgAttRpt.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void btnEnterLeave_Click_1(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Microsoft.Office.Interop.Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            var CR = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[1, 1];
            CR.Select();
            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }
    }

}

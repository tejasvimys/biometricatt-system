﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Seeds_Billing_App_Utilities;
using BiometricAttSystem.Entities;
using System.Configuration;
using System.Data;

namespace BiometricAttSystem.DataLayer
{
    class Leave
    {
        public MySqlConnection ReturnConnectionString()
        {
            if (ConfigurationManager.ConnectionStrings == null) return null;
            var myConnectionString = new MySqlConnection(ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
            return myConnectionString;
        }
        public List<Employee> GetEmployeeList()
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            var employee = new List<Employee>();
            try
            {
                using (var cmd = new MySqlCommand("Get_EmployeeIdandNames", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Employee()
                            {
                                EnrolmentId = Convert.ToInt32(reader["employeeenrolmentid"].ToString()),
                                EmployeeFName = reader["empName"].ToString(),
                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public List<LeaveType> GetLeaveType()
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            var employee = new List<LeaveType>();
            try
            {
                using (var cmd = new MySqlCommand("Get_LeaveTypes", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new LeaveType()
                            {
                                LeaveTypeId = Convert.ToInt32(reader["leavetypemasterid"].ToString()),
                                TypeOfLeave = reader["leavetype"].ToString(),
                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public int GetNoofLeavesAvailed(int empId, string leavetype)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("Get_Count_Of_Leaves", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("emp", empId);
                    cmd.Parameters.AddWithValue("leavetyp", leavetype);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            i = reader[0].ToString() == string.Empty ? 0 : Convert.ToInt32(reader[0].ToString());
                        }

                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public void EnterLeave(Entities.Leave objLeave)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            try
            {
                using (var cmd = new MySqlCommand("Insert_leave_for_employee", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("eusrid", objLeave.empid);
                    cmd.Parameters.AddWithValue("leavefrmdate", DateTime.Parse(objLeave.leavefromdate));
                    cmd.Parameters.AddWithValue("leavetdate",DateTime.Parse(objLeave.leaveToDate));
                    cmd.Parameters.AddWithValue("ttldays", objLeave.NoofDays);
                    cmd.Parameters.AddWithValue("leavetype", objLeave.TypeOfLeave);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public int GetNoOfLeavesByLeaveType(string leavetype)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("Get_Total_Leaves_Per_LeaveType", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("leaveTyp", leavetype);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            i = reader[0].ToString() == string.Empty ? 0 : Convert.ToInt32(reader[0].ToString());
                        }

                        return i;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }
    }
}

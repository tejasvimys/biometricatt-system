﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Seeds_Billing_App_Utilities;
using BiometricAttSystem.Entities;
using System.Configuration;
using System.Data;

namespace BiometricAttSystem.DataLayer
{
    public class Reports
    {
        public MySqlConnection ReturnConnectionString()
        {
            if (ConfigurationManager.ConnectionStrings == null) return null;
            var myConnectionString = new MySqlConnection(ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
            return myConnectionString;
        }
        public List<Entities.Attendance> GetAttendanceReport(int month)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            var employee = new List<Entities.Attendance>();
            try
            {
                using (var cmd = new MySqlCommand("Get_AttendanceRecordsByMonth", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("monthVal", month);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Entities.Attendance()
                            {
                                EmployeeEnrolmentId = reader["empUserid"].ToString(),
                                DateIn = reader["datein"].ToString(),
                                TimeIn = reader["timein"].ToString(),
                                DateOut = reader["dateout"].ToString(),
                                TimeOut = reader["timeout"].ToString(),
                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public List<Entities.Attendance> GetAttendanceTotalReport(int month)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            var employee = new List<Entities.Attendance>();
            try
            {
                using (var cmd = new MySqlCommand("Get_Sum_Attendance_Report_Per_Month", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("monthval", month);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Entities.Attendance()
                            {
                                EmployeeEnrolmentId = reader["empUserid"].ToString(),
                                DaysAttended = Convert.ToInt32(reader["noofdaysattended"].ToString())

                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public List<Entities.Leave> GetLeaveReport(int month)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            var employee = new List<Entities.Leave>();
            try
            {
                using (var cmd = new MySqlCommand("Get_Leave_Report", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("monthval", month);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Entities.Leave()
                            {
                                EmployeeFName = reader["empname"].ToString(),
                                Department = reader["empDept"].ToString(),
                                Designation = reader["empdesig"].ToString(),
                                TypeOfLeave = reader["TypeOfLeave"].ToString(),
                                NoofDays = int.Parse(reader["totalDays"].ToString()),
                                leavefromdate = reader["leavefromdate"].ToString(),
                                leaveToDate = reader["leavetodate"].ToString(),
                                EmployeeId = reader["empuserid"].ToString(),

                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }
    }
}

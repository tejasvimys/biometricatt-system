﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using Seeds_Billing_App_Utilities;
using BiometricAttSystem.Entities;

namespace BiometricAttSystem.DataLayer
{
   public class SaveEmployee
    {
        public MySqlConnection ReturnConnectionString()
        {
            if (ConfigurationManager.ConnectionStrings == null) return null;
            var myConnectionString = new MySqlConnection(ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
            return myConnectionString;
        }

        public string SaveEmployeeDetails(Employee empDetails)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("InsertEmployeeDetails", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("aadharno", empDetails.AadharNo);
                    cmd.Parameters.AddWithValue("EmployeeId", empDetails.EmployeeId);
                    cmd.Parameters.AddWithValue("firstName", empDetails.EmployeeFName);
                    cmd.Parameters.AddWithValue("lastName", empDetails.EmployeeLName);
                    cmd.Parameters.AddWithValue("department", empDetails.Department);
                    cmd.Parameters.AddWithValue("designation", empDetails.Designation);

                    cmd.Parameters.Add("pic", MySqlDbType.LongBlob).Value = empDetails.empPic;
                    cmd.Parameters.Add("temp11", MySqlDbType.LongBlob).Value = empDetails.f1t1;
                    cmd.Parameters.Add("temp12", MySqlDbType.LongBlob).Value = empDetails.f1t2;
                    cmd.Parameters.Add("temp21", MySqlDbType.LongBlob).Value = empDetails.f2t1;
                    cmd.Parameters.Add("temp22", MySqlDbType.LongBlob).Value = empDetails.f2t2;
                    i = cmd.ExecuteNonQuery();
                }

                return i.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                return ex.ToString();
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }

        }   

        public string UpdateEmployeeDetails(int eId, string empid, string fname, string lname, string dept, string desig, Byte[] empPic, Byte[] f1t1, Byte[] f1t2, Byte[] f2t1, Byte[] f2t2)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("UpdateEmployeeDetails", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("eid", eId);
                    cmd.Parameters.AddWithValue("employeeId", empid);
                    cmd.Parameters.AddWithValue("firstName", fname);
                    cmd.Parameters.AddWithValue("lastName", lname);
                    cmd.Parameters.AddWithValue("department", dept);
                    cmd.Parameters.AddWithValue("designation", desig);
                    cmd.Parameters.AddWithValue("empPhoto", empPic);
                    cmd.Parameters.AddWithValue("f1t1", f1t1);
                    cmd.Parameters.AddWithValue("f1t2", f1t2);
                    cmd.Parameters.AddWithValue("f2t1", f2t1);
                    cmd.Parameters.AddWithValue("f2t2", f2t2);
                    i = cmd.ExecuteNonQuery();
                }



                return i.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                return ex.ToString();
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }

        }

        public string DeleteEmployeeDetails(int eId)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("DeleteEmployeeDetails", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("eid", eId);
                    i = cmd.ExecuteNonQuery();
                }
                return i.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                return ex.ToString();
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Seeds_Billing_App_Utilities;
using BiometricAttSystem.Entities;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

namespace BiometricAttSystem.DataLayer
{
    public class Attendance
    {
        public MySqlConnection ReturnConnectionString()
        {
            if (ConfigurationManager.ConnectionStrings == null) return null;
            var myConnectionString = new MySqlConnection(ConfigurationManager.ConnectionStrings["connstring"].ConnectionString);
            return myConnectionString;
        }
        public Employee GetEmployeeDetails(string enrolmentNumber)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("GetEmployeeDetails", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("enrolmentno", enrolmentNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Employee()
                            {
                                EnrolmentId = Convert.ToInt32(reader["employeeenrolmentid"].ToString()),
                                EmployeeId = reader["empUserId"].ToString(),
                                EmployeeFName = reader["empname"].ToString(),
                                EmployeeLName = reader["emplastname"].ToString(),
                                Department = reader["empDept"].ToString(),
                                Designation = reader["empdesig"].ToString(),
                                empPic = (Byte[])(reader["empphoto"]),
                                f1t1 = (Byte[])(reader["empf1t1"]),
                                f1t2 = (Byte[])(reader["empf1t2"]),
                                f2t1 = (Byte[])(reader["empf2t1"]),
                                f2t2 = (Byte[])(reader["empf2t2"]),
                            };
                            return kby;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public bool GetIfEmployeePunchedAttendance(string enrolmentNumber)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            try
            {
                using (var cmd = new MySqlCommand("GetIfAttEnteredFortheDay", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("enrolmentid", enrolmentNumber);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader.HasRows)
                            {
                                return true;
                            }
                        }
                    }
                }

                return false    ;
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public void EnterAttendance(string empId)
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            try
            {
                using (var cmd = new MySqlCommand("InsertTimeInRecord", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("eusrid", empId);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public void UpdateAttendance(string empId)
        {
            var connectionString = new ConnectionString();  
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            try
            {
                using (var cmd = new MySqlCommand("updateTimeoutRecord", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("eusrid", empId);
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }

        public List<Employee> GetEmployeeEnrolmentandFingerDetails()
        {
            var connectionString = new ConnectionString();
            var mySqlConnection = ReturnConnectionString();
            mySqlConnection.Open();
            int i = 0;
            var employee = new List<Employee>();
            try
            {
                using (var cmd = new MySqlCommand("Get_Employee_Enrolment_With_FP", mySqlConnection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var kby = new Employee()
                            {
                                EnrolmentId = Convert.ToInt32(reader["employeeenrolmentid"].ToString()),
                                EmployeeId = reader["empUserId"].ToString(),     
                                f1t1 = (Byte[])(reader["empf1t1"]),
                                f1t2 = (Byte[])(reader["empf1t2"]),
                                f2t1 = (Byte[])(reader["empf2t1"]),
                                f2t2 = (Byte[])(reader["empf2t2"]),
                                EmployeeFName = reader["employeeName"].ToString(),
                                Department = reader["empDept"].ToString(),
                                Designation = reader["empdesig"].ToString(),
                            };
                            employee.Add(kby);
                        }

                        return employee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw;
            }
            finally
            {
                mySqlConnection.Close();
                mySqlConnection.Dispose();
            }
        }
    }
}

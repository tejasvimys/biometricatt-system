﻿namespace BiometricAttSystem
{
    partial class frmBAS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.eNROLLUSERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gETUSERATTENDANCEREPORTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lEAVEMANAGEMENTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eNTERLEAVEDETAILSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gETLEAVEREPORTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEPORTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aTTENDANCEREPORTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNROLLUSERToolStripMenuItem,
            this.lEAVEMANAGEMENTToolStripMenuItem,
            this.rEPORTSToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1234, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // eNROLLUSERToolStripMenuItem
            // 
            this.eNROLLUSERToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem,
            this.gETUSERATTENDANCEREPORTToolStripMenuItem});
            this.eNROLLUSERToolStripMenuItem.Name = "eNROLLUSERToolStripMenuItem";
            this.eNROLLUSERToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.eNROLLUSERToolStripMenuItem.Text = "ENROLL USER";
            // 
            // eNROLLUSERFORATTENDANCEToolStripMenuItem
            // 
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem.Name = "eNROLLUSERFORATTENDANCEToolStripMenuItem";
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem.Text = "ENROLL USER FOR ATTENDANCE";
            this.eNROLLUSERFORATTENDANCEToolStripMenuItem.Click += new System.EventHandler(this.eNROLLUSERFORATTENDANCEToolStripMenuItem_Click);
            // 
            // gETUSERATTENDANCEREPORTToolStripMenuItem
            // 
            this.gETUSERATTENDANCEREPORTToolStripMenuItem.Name = "gETUSERATTENDANCEREPORTToolStripMenuItem";
            this.gETUSERATTENDANCEREPORTToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.gETUSERATTENDANCEREPORTToolStripMenuItem.Text = "GET USER ATTENDANCE REPORT";
            // 
            // lEAVEMANAGEMENTToolStripMenuItem
            // 
            this.lEAVEMANAGEMENTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eNTERLEAVEDETAILSToolStripMenuItem,
            this.gETLEAVEREPORTSToolStripMenuItem});
            this.lEAVEMANAGEMENTToolStripMenuItem.Name = "lEAVEMANAGEMENTToolStripMenuItem";
            this.lEAVEMANAGEMENTToolStripMenuItem.Size = new System.Drawing.Size(138, 20);
            this.lEAVEMANAGEMENTToolStripMenuItem.Text = "LEAVE MANAGEMENT";
            // 
            // eNTERLEAVEDETAILSToolStripMenuItem
            // 
            this.eNTERLEAVEDETAILSToolStripMenuItem.Name = "eNTERLEAVEDETAILSToolStripMenuItem";
            this.eNTERLEAVEDETAILSToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.eNTERLEAVEDETAILSToolStripMenuItem.Text = "ENTER LEAVE DETAILS";
            this.eNTERLEAVEDETAILSToolStripMenuItem.Click += new System.EventHandler(this.eNTERLEAVEDETAILSToolStripMenuItem_Click);
            // 
            // gETLEAVEREPORTSToolStripMenuItem
            // 
            this.gETLEAVEREPORTSToolStripMenuItem.Name = "gETLEAVEREPORTSToolStripMenuItem";
            this.gETLEAVEREPORTSToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.gETLEAVEREPORTSToolStripMenuItem.Text = "GET LEAVE REPORTS";
            this.gETLEAVEREPORTSToolStripMenuItem.Click += new System.EventHandler(this.gETLEAVEREPORTSToolStripMenuItem_Click);
            // 
            // rEPORTSToolStripMenuItem
            // 
            this.rEPORTSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aTTENDANCEREPORTToolStripMenuItem,
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem});
            this.rEPORTSToolStripMenuItem.Name = "rEPORTSToolStripMenuItem";
            this.rEPORTSToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.rEPORTSToolStripMenuItem.Text = "REPORTS";
            // 
            // aTTENDANCEREPORTToolStripMenuItem
            // 
            this.aTTENDANCEREPORTToolStripMenuItem.Name = "aTTENDANCEREPORTToolStripMenuItem";
            this.aTTENDANCEREPORTToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.aTTENDANCEREPORTToolStripMenuItem.Text = "ATTENDANCE REPORT";
            this.aTTENDANCEREPORTToolStripMenuItem.Click += new System.EventHandler(this.aTTENDANCEREPORTToolStripMenuItem_Click);
            // 
            // nOOFDAYSATTENDEDREPORTToolStripMenuItem
            // 
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem.Name = "nOOFDAYSATTENDEDREPORTToolStripMenuItem";
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem.Text = "NO OF DAYS ATTENDED REPORT";
            this.nOOFDAYSATTENDEDREPORTToolStripMenuItem.Click += new System.EventHandler(this.nOOFDAYSATTENDEDREPORTToolStripMenuItem_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 24);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1234, 463);
            this.pnlMain.TabIndex = 1;
            // 
            // frmBAS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1234, 487);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmBAS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bio Metric Attendance System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem eNROLLUSERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eNROLLUSERFORATTENDANCEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gETUSERATTENDANCEREPORTToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.ToolStripMenuItem lEAVEMANAGEMENTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eNTERLEAVEDETAILSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gETLEAVEREPORTSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEPORTSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aTTENDANCEREPORTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nOOFDAYSATTENDEDREPORTToolStripMenuItem;
    }
}


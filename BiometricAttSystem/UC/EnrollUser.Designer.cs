﻿namespace BiometricAttSystem.UC
{
    partial class EnrollUser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnef1 = new System.Windows.Forms.Button();
            this.btnef2 = new System.Windows.Forms.Button();
            this.btnef3 = new System.Windows.Forms.Button();
            this.btnTf = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNotification = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.comboBoxSecuLevel_V = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxSecuLevel_R = new System.Windows.Forms.ComboBox();
            this.groupBoxUsbDevs = new System.Windows.Forms.GroupBox();
            this.OpenDeviceBtn = new System.Windows.Forms.Button();
            this.EnumerateBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxDeviceName = new System.Windows.Forms.ComboBox();
            this.btnTestMatch = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBoxUsbDevs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(52, 288);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(155, 154);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(236, 288);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(155, 154);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(411, 288);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(155, 154);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(877, 288);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(155, 154);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // btnef1
            // 
            this.btnef1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnef1.Location = new System.Drawing.Point(52, 492);
            this.btnef1.Name = "btnef1";
            this.btnef1.Size = new System.Drawing.Size(155, 23);
            this.btnef1.TabIndex = 4;
            this.btnef1.Text = "Enrol Template 1 for Finger 1";
            this.btnef1.UseVisualStyleBackColor = true;
            this.btnef1.Click += new System.EventHandler(this.btnef1_Click);
            // 
            // btnef2
            // 
            this.btnef2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnef2.Location = new System.Drawing.Point(236, 492);
            this.btnef2.Name = "btnef2";
            this.btnef2.Size = new System.Drawing.Size(155, 23);
            this.btnef2.TabIndex = 5;
            this.btnef2.Text = "Enrol Finger 2";
            this.btnef2.UseVisualStyleBackColor = true;
            this.btnef2.Click += new System.EventHandler(this.btnef2_Click);
            // 
            // btnef3
            // 
            this.btnef3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnef3.Location = new System.Drawing.Point(411, 492);
            this.btnef3.Name = "btnef3";
            this.btnef3.Size = new System.Drawing.Size(155, 23);
            this.btnef3.TabIndex = 6;
            this.btnef3.Text = "Enrol Finger 3";
            this.btnef3.UseVisualStyleBackColor = true;
            this.btnef3.Click += new System.EventHandler(this.btnef3_Click);
            // 
            // btnTf
            // 
            this.btnTf.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTf.Location = new System.Drawing.Point(877, 478);
            this.btnTf.Name = "btnTf";
            this.btnTf.Size = new System.Drawing.Size(155, 23);
            this.btnTf.TabIndex = 7;
            this.btnTf.Text = "Test Finger";
            this.btnTf.UseVisualStyleBackColor = true;
            this.btnTf.Click += new System.EventHandler(this.btnTf_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 260);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Template 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(244, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Template 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(408, 260);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Template 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(874, 260);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Test Template";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(22, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(687, 217);
            this.panel1.TabIndex = 12;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(52, 454);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(155, 23);
            this.progressBar1.TabIndex = 13;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(236, 454);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(155, 23);
            this.progressBar2.TabIndex = 14;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(411, 454);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(155, 23);
            this.progressBar3.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(812, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Notification Message:";
            // 
            // lblNotification
            // 
            this.lblNotification.AutoSize = true;
            this.lblNotification.Location = new System.Drawing.Point(738, 179);
            this.lblNotification.Name = "lblNotification";
            this.lblNotification.Size = new System.Drawing.Size(0, 13);
            this.lblNotification.TabIndex = 17;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.comboBoxSecuLevel_V);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.comboBoxSecuLevel_R);
            this.groupBox6.Location = new System.Drawing.Point(735, 82);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(414, 56);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Security Level";
            // 
            // comboBoxSecuLevel_V
            // 
            this.comboBoxSecuLevel_V.Items.AddRange(new object[] {
            "LOWEST",
            "LOWER",
            "LOW",
            "BELOW_NORMAL",
            "NORMAL",
            "ABOVE_NORMAL",
            "HIGH",
            "HIGHER",
            "HIGHEST"});
            this.comboBoxSecuLevel_V.Location = new System.Drawing.Point(272, 24);
            this.comboBoxSecuLevel_V.Name = "comboBoxSecuLevel_V";
            this.comboBoxSecuLevel_V.Size = new System.Drawing.Size(112, 21);
            this.comboBoxSecuLevel_V.TabIndex = 24;
            this.comboBoxSecuLevel_V.Text = "NORMAL";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(208, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 24);
            this.label14.TabIndex = 23;
            this.label14.Text = "Verification";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 24);
            this.label6.TabIndex = 22;
            this.label6.Text = "Registration";
            // 
            // comboBoxSecuLevel_R
            // 
            this.comboBoxSecuLevel_R.Items.AddRange(new object[] {
            "LOWEST",
            "LOWER",
            "LOW",
            "BELOW_NORMAL",
            "NORMAL",
            "ABOVE_NORMAL",
            "HIGH",
            "HIGHER",
            "HIGHEST"});
            this.comboBoxSecuLevel_R.Location = new System.Drawing.Point(80, 24);
            this.comboBoxSecuLevel_R.Name = "comboBoxSecuLevel_R";
            this.comboBoxSecuLevel_R.Size = new System.Drawing.Size(112, 21);
            this.comboBoxSecuLevel_R.TabIndex = 21;
            this.comboBoxSecuLevel_R.Text = "NORMAL";
            // 
            // groupBoxUsbDevs
            // 
            this.groupBoxUsbDevs.Controls.Add(this.OpenDeviceBtn);
            this.groupBoxUsbDevs.Controls.Add(this.EnumerateBtn);
            this.groupBoxUsbDevs.Controls.Add(this.label7);
            this.groupBoxUsbDevs.Controls.Add(this.comboBoxDeviceName);
            this.groupBoxUsbDevs.Location = new System.Drawing.Point(735, 19);
            this.groupBoxUsbDevs.Name = "groupBoxUsbDevs";
            this.groupBoxUsbDevs.Size = new System.Drawing.Size(414, 57);
            this.groupBoxUsbDevs.TabIndex = 32;
            this.groupBoxUsbDevs.TabStop = false;
            this.groupBoxUsbDevs.Text = "USB";
            // 
            // OpenDeviceBtn
            // 
            this.OpenDeviceBtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.OpenDeviceBtn.Location = new System.Drawing.Point(245, 19);
            this.OpenDeviceBtn.Name = "OpenDeviceBtn";
            this.OpenDeviceBtn.Size = new System.Drawing.Size(72, 24);
            this.OpenDeviceBtn.TabIndex = 13;
            this.OpenDeviceBtn.Text = "Init";
            this.OpenDeviceBtn.UseVisualStyleBackColor = false;
            this.OpenDeviceBtn.Click += new System.EventHandler(this.OpenDeviceBtn_Click);
            // 
            // EnumerateBtn
            // 
            this.EnumerateBtn.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.EnumerateBtn.Location = new System.Drawing.Point(329, 19);
            this.EnumerateBtn.Name = "EnumerateBtn";
            this.EnumerateBtn.Size = new System.Drawing.Size(72, 24);
            this.EnumerateBtn.TabIndex = 12;
            this.EnumerateBtn.Text = "Enumerate";
            this.EnumerateBtn.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(4, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 24);
            this.label7.TabIndex = 11;
            this.label7.Text = "Device Name";
            // 
            // comboBoxDeviceName
            // 
            this.comboBoxDeviceName.Location = new System.Drawing.Point(81, 19);
            this.comboBoxDeviceName.Name = "comboBoxDeviceName";
            this.comboBoxDeviceName.Size = new System.Drawing.Size(152, 21);
            this.comboBoxDeviceName.TabIndex = 10;
            // 
            // btnTestMatch
            // 
            this.btnTestMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestMatch.Location = new System.Drawing.Point(877, 201);
            this.btnTestMatch.Name = "btnTestMatch";
            this.btnTestMatch.Size = new System.Drawing.Size(155, 23);
            this.btnTestMatch.TabIndex = 33;
            this.btnTestMatch.Text = "Test Match";
            this.btnTestMatch.UseVisualStyleBackColor = true;
            this.btnTestMatch.Click += new System.EventHandler(this.btnTestMatch_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(595, 288);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(155, 154);
            this.pictureBox5.TabIndex = 34;
            this.pictureBox5.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(592, 260);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Template 4";
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(595, 454);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(155, 23);
            this.progressBar4.TabIndex = 36;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(595, 492);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Enrol Finger 3";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // EnrollUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.progressBar4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.btnTestMatch);
            this.Controls.Add(this.groupBoxUsbDevs);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.lblNotification);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnTf);
            this.Controls.Add(this.btnef3);
            this.Controls.Add(this.btnef2);
            this.Controls.Add(this.btnef1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "EnrollUser";
            this.Size = new System.Drawing.Size(1163, 527);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBoxUsbDevs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btnef1;
        private System.Windows.Forms.Button btnef2;
        private System.Windows.Forms.Button btnef3;
        private System.Windows.Forms.Button btnTf;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNotification;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBoxSecuLevel_V;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxSecuLevel_R;
        private System.Windows.Forms.GroupBox groupBoxUsbDevs;
        private System.Windows.Forms.Button OpenDeviceBtn;
        private System.Windows.Forms.Button EnumerateBtn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxDeviceName;
        private System.Windows.Forms.Button btnTestMatch;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.Button button1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using SecuGen.FDxSDKPro.Windows;

namespace BiometricAttSystem.UC
{
    public partial class EnrollUser : UserControl
    {
        public SGFingerPrintManager m_FPM;

        private bool m_LedOn = false;
        private Int32 m_ImageWidth;
        private Int32 m_ImageHeight;
        private Byte[] m_RegMin1;
        private Byte[] m_RegMin2;
        private Byte[] m_VrfMin;
        private SGFPMDeviceList[] m_DevList; // Used for EnumerateDevice
        public EnrollUser()
        {
            InitializeComponent();
        }

        private void btnef1_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Int32 iError;
                Byte[] fp_image;
                Int32 img_qlty;

                fp_image = new Byte[m_ImageWidth * m_ImageHeight];
                img_qlty = 0;

                iError = m_FPM.GetImage(fp_image);

                m_FPM.GetImageQuality(m_ImageWidth, m_ImageHeight, fp_image, ref img_qlty);
                progressBar1.Value = img_qlty;

                if (iError == (Int32)SGFPMError.ERROR_NONE)
                {
                    DrawImage(fp_image, pictureBox1);
                    iError = m_FPM.CreateTemplate(fp_image, m_RegMin1);

                    if (iError == (Int32)SGFPMError.ERROR_NONE)
                        MessageBox.Show("First image is captured");
                    else
                        DisplayError("CreateTemplate()", iError);
                }
                else
                    DisplayError("GetImage()", iError);

                Cursor.Current = Cursors.Default;
            }

            catch
            {

            }
        }

        private void btnef2_Click(object sender, EventArgs e)
        {
            try
            {

            }

            catch
            {

            }
        }

        private void btnef3_Click(object sender, EventArgs e)
        {
            try
            {

            }

            catch
            {

            }
        }

        private void btnTf_Click(object sender, EventArgs e)
        {
            try
            {

            }

            catch
            {

            }
        }

        private void DrawImage(Byte[] imgData, PictureBox picBox)
        {
            int colorval;
            Bitmap bmp = new Bitmap(m_ImageWidth, m_ImageHeight);
            picBox.Image = (Image)bmp;

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    colorval = (int)imgData[(j * m_ImageWidth) + i];
                    bmp.SetPixel(i, j, Color.FromArgb(colorval, colorval, colorval));
                }
            }
            picBox.Refresh();
        }

        void DisplayError(string funcName, int iError)
        {
            string text = "";

            switch (iError)
            {
                case 0:                             //SGFDX_ERROR_NONE				= 0,
                    text = "Error none";
                    break;

                case 1:                             //SGFDX_ERROR_CREATION_FAILED	= 1,
                    text = "Can not create object";
                    break;

                case 2:                             //   SGFDX_ERROR_FUNCTION_FAILED	= 2,
                    text = "Function Failed";
                    break;

                case 3:                             //   SGFDX_ERROR_INVALID_PARAM	= 3,
                    text = "Invalid Parameter";
                    break;

                case 4:                          //   SGFDX_ERROR_NOT_USED			= 4,
                    text = "Not used function";
                    break;

                case 5:                                //SGFDX_ERROR_DLLLOAD_FAILED	= 5,
                    text = "Can not create object";
                    break;

                case 6:                                //SGFDX_ERROR_DLLLOAD_FAILED_DRV	= 6,
                    text = "Can not load device driver";
                    break;
                case 7:                                //SGFDX_ERROR_DLLLOAD_FAILED_ALGO = 7,
                    text = "Can not load sgfpamx.dll";
                    break;

                case 51:                //SGFDX_ERROR_SYSLOAD_FAILED	   = 51,	// system file load fail
                    text = "Can not load driver kernel file";
                    break;

                case 52:                //SGFDX_ERROR_INITIALIZE_FAILED  = 52,   // chip initialize fail
                    text = "Failed to initialize the device";
                    break;

                case 53:                //SGFDX_ERROR_LINE_DROPPED		   = 53,   // image data drop
                    text = "Data transmission is not good";
                    break;

                case 54:                //SGFDX_ERROR_TIME_OUT			   = 54,   // getliveimage timeout error
                    text = "Time out";
                    break;

                case 55:                //SGFDX_ERROR_DEVICE_NOT_FOUND	= 55,   // device not found
                    text = "Device not found";
                    break;

                case 56:                //SGFDX_ERROR_DRVLOAD_FAILED	   = 56,   // dll file load fail
                    text = "Can not load driver file";
                    break;

                case 57:                //SGFDX_ERROR_WRONG_IMAGE		   = 57,   // wrong image
                    text = "Wrong Image";
                    break;

                case 58:                //SGFDX_ERROR_LACK_OF_BANDWIDTH  = 58,   // USB Bandwith Lack Error
                    text = "Lack of USB Bandwith";
                    break;

                case 59:                //SGFDX_ERROR_DEV_ALREADY_OPEN	= 59,   // Device Exclusive access Error
                    text = "Device is already opened";
                    break;

                case 60:                //SGFDX_ERROR_GETSN_FAILED		   = 60,   // Fail to get Device Serial Number
                    text = "Device serial number error";
                    break;

                case 61:                //SGFDX_ERROR_UNSUPPORTED_DEV		   = 61,   // Unsupported device
                    text = "Unsupported device";
                    break;

                // Extract & Verification error
                case 101:                //SGFDX_ERROR_FEAT_NUMBER		= 101, // utoo small number of minutiae
                    text = "The number of minutiae is too small";
                    break;

                case 102:                //SGFDX_ERROR_INVALID_TEMPLATE_TYPE		= 102, // wrong template type
                    text = "Template is invalid";
                    break;

                case 103:                //SGFDX_ERROR_INVALID_TEMPLATE1		= 103, // wrong template type
                    text = "1st template is invalid";
                    break;

                case 104:                //SGFDX_ERROR_INVALID_TEMPLATE2		= 104, // vwrong template type
                    text = "2nd template is invalid";
                    break;

                case 105:                //SGFDX_ERROR_EXTRACT_FAIL		= 105, // extraction fail
                    text = "Minutiae extraction failed";
                    break;

                case 106:                //SGFDX_ERROR_MATCH_FAIL		= 106, // matching  fail
                    text = "Matching failed";
                    break;

            }

            text = funcName + " Error # " + iError + " :" + text;
            lblNotification.Text = text;
        }

        private void OpenDeviceBtn_Click(object sender, EventArgs e)
        {
            Int32 iError;
           
           var port_addr = SGFPMPortAddr.USB_AUTO_DETECT;
            iError = m_FPM.OpenDevice(Convert.ToInt32(port_addr));
            if (iError == (Int32)SGFPMError.ERROR_NONE)
                lblNotification.Text = "Initialization Success";
            else
                lblNotification.Text = "OpenDevice() Error : " + iError;

            //if (m_FPM.NumberOfDevice == 0)
            //    return;

            //SGFPMDeviceName device_name;
            //Int32 device_id;

            //Int32 numberOfDevices = comboBoxDeviceName.Items.Count;
            //Int32 deviceSelected = comboBoxDeviceName.SelectedIndex;
            //Boolean autoSelection = (deviceSelected == (numberOfDevices - 1));  // Last index

            //if (autoSelection)
            //{
            //    // Order of search: Hamster IV(HFDU04) -> Plus(HFDU03) -> III (HFDU02)
            //    device_name = SGFPMDeviceName.DEV_AUTO;

            //    device_id = (Int32)(SGFPMPortAddr.USB_AUTO_DETECT);
            //}
            //else
            //{
            //    device_name = m_DevList[deviceSelected].DevName;
            //    device_id = m_DevList[deviceSelected].DevID;
            //}

            //Int32 iError = OpenDevice(device_name, device_id);
        }

        private void btnTestMatch_Click(object sender, EventArgs e)
        {

        }

        //private Int32 OpenDevice(SGFPMDeviceName device_name, Int32 device_id)
        //{
        //    Int32 iError = m_FPM.Init(device_name);
        //    iError = m_FPM.OpenDevice(device_id);

        //    if (iError == (Int32)SGFPMError.ERROR_NONE)
        //    {
        //        lblNotification.Text = "Initialization Success";

        //    }
        //    else
        //        DisplayError("OpenDevice()", iError);
        //    return iError;
        //}

        //private void CloseDevice()
        //{
        //    m_FPM.CloseDevice();
        //}
    }
}

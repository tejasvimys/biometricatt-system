﻿namespace BiometricAttSystem
{
    partial class EnterLeave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNotification = new System.Windows.Forms.Label();
            this.btnEnterLeave = new System.Windows.Forms.Button();
            this.cmbEmp = new System.Windows.Forms.ComboBox();
            this.cmbLeaveType = new System.Windows.Forms.ComboBox();
            this.dtFrm = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNoOfDays = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCalcLeaves = new System.Windows.Forms.Label();
            this.txtnoofdaysremain = new System.Windows.Forms.TextBox();
            this.txtNoofDaysTaken = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNotification
            // 
            this.lblNotification.AutoSize = true;
            this.lblNotification.Location = new System.Drawing.Point(1453, 249);
            this.lblNotification.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblNotification.Name = "lblNotification";
            this.lblNotification.Size = new System.Drawing.Size(0, 20);
            this.lblNotification.TabIndex = 50;
            // 
            // btnEnterLeave
            // 
            this.btnEnterLeave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnterLeave.Location = new System.Drawing.Point(506, 395);
            this.btnEnterLeave.Margin = new System.Windows.Forms.Padding(5);
            this.btnEnterLeave.Name = "btnEnterLeave";
            this.btnEnterLeave.Size = new System.Drawing.Size(232, 80);
            this.btnEnterLeave.TabIndex = 37;
            this.btnEnterLeave.Text = "SAVE LEAVE DETAILS";
            this.btnEnterLeave.UseVisualStyleBackColor = true;
            this.btnEnterLeave.Click += new System.EventHandler(this.btnef1_Click_1);
            // 
            // cmbEmp
            // 
            this.cmbEmp.FormattingEnabled = true;
            this.cmbEmp.Location = new System.Drawing.Point(248, 69);
            this.cmbEmp.Margin = new System.Windows.Forms.Padding(5);
            this.cmbEmp.Name = "cmbEmp";
            this.cmbEmp.Size = new System.Drawing.Size(391, 28);
            this.cmbEmp.TabIndex = 51;
            // 
            // cmbLeaveType
            // 
            this.cmbLeaveType.FormattingEnabled = true;
            this.cmbLeaveType.Location = new System.Drawing.Point(248, 140);
            this.cmbLeaveType.Margin = new System.Windows.Forms.Padding(5);
            this.cmbLeaveType.Name = "cmbLeaveType";
            this.cmbLeaveType.Size = new System.Drawing.Size(391, 28);
            this.cmbLeaveType.TabIndex = 52;
            this.cmbLeaveType.SelectedIndexChanged += new System.EventHandler(this.cmbLeaveType_SelectedIndexChanged);
            this.cmbLeaveType.SelectionChangeCommitted += new System.EventHandler(this.cmbLeaveType_SelectionChangeCommitted);
            // 
            // dtFrm
            // 
            this.dtFrm.Location = new System.Drawing.Point(248, 202);
            this.dtFrm.Margin = new System.Windows.Forms.Padding(5);
            this.dtFrm.Name = "dtFrm";
            this.dtFrm.Size = new System.Drawing.Size(391, 26);
            this.dtFrm.TabIndex = 53;
            // 
            // dtTo
            // 
            this.dtTo.Location = new System.Drawing.Point(248, 267);
            this.dtTo.Margin = new System.Windows.Forms.Padding(5);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(391, 26);
            this.dtTo.TabIndex = 54;
            this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(60, 72);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 20);
            this.label1.TabIndex = 56;
            this.label1.Text = "SELECT EMPLOYEE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 143);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 20);
            this.label2.TabIndex = 57;
            this.label2.Text = "SELECT LEAVE TYPE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-1, 202);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 20);
            this.label3.TabIndex = 58;
            this.label3.Text = "ENTER LEAVE FROM DATE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 272);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 20);
            this.label4.TabIndex = 59;
            this.label4.Text = "ENTER LEAVE TO DATE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(660, 117);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(265, 20);
            this.label5.TabIndex = 60;
            this.label5.Text = "NO OF DAYS LEAVE APPLIED:";
            // 
            // lblNoOfDays
            // 
            this.lblNoOfDays.AutoSize = true;
            this.lblNoOfDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoOfDays.Location = new System.Drawing.Point(935, 70);
            this.lblNoOfDays.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblNoOfDays.Name = "lblNoOfDays";
            this.lblNoOfDays.Size = new System.Drawing.Size(0, 20);
            this.lblNoOfDays.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(702, 161);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 20);
            this.label6.TabIndex = 62;
            this.label6.Text = "NO OF DAYS REMAINING";
            // 
            // lblCalcLeaves
            // 
            this.lblCalcLeaves.AutoSize = true;
            this.lblCalcLeaves.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalcLeaves.Location = new System.Drawing.Point(999, 209);
            this.lblCalcLeaves.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblCalcLeaves.Name = "lblCalcLeaves";
            this.lblCalcLeaves.Size = new System.Drawing.Size(0, 20);
            this.lblCalcLeaves.TabIndex = 63;
            // 
            // txtnoofdaysremain
            // 
            this.txtnoofdaysremain.Location = new System.Drawing.Point(933, 155);
            this.txtnoofdaysremain.Name = "txtnoofdaysremain";
            this.txtnoofdaysremain.Size = new System.Drawing.Size(315, 26);
            this.txtnoofdaysremain.TabIndex = 64;
            // 
            // txtNoofDaysTaken
            // 
            this.txtNoofDaysTaken.Location = new System.Drawing.Point(933, 111);
            this.txtNoofDaysTaken.MaxLength = 2;
            this.txtNoofDaysTaken.Name = "txtNoofDaysTaken";
            this.txtNoofDaysTaken.Size = new System.Drawing.Size(315, 26);
            this.txtNoofDaysTaken.TabIndex = 65;
            this.txtNoofDaysTaken.TextChanged += new System.EventHandler(this.txtNoofDaysTaken_TextChanged);
            this.txtNoofDaysTaken.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoofDaysTaken_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(699, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(226, 20);
            this.label7.TabIndex = 66;
            this.label7.Text = "TOTAL LEAVES AVAILED:";
            // 
            // EnterLeave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1260, 741);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNoofDaysTaken);
            this.Controls.Add(this.txtnoofdaysremain);
            this.Controls.Add(this.lblCalcLeaves);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblNoOfDays);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.dtFrm);
            this.Controls.Add(this.cmbLeaveType);
            this.Controls.Add(this.cmbEmp);
            this.Controls.Add(this.lblNotification);
            this.Controls.Add(this.btnEnterLeave);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "EnterLeave";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ENTER LEAVE";
            this.Load += new System.EventHandler(this.EnrolUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNotification;
        private System.Windows.Forms.Button btnEnterLeave;
        private System.Windows.Forms.ComboBox cmbEmp;
        private System.Windows.Forms.ComboBox cmbLeaveType;
        private System.Windows.Forms.DateTimePicker dtFrm;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNoOfDays;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCalcLeaves;
        private System.Windows.Forms.TextBox txtnoofdaysremain;
        private System.Windows.Forms.TextBox txtNoofDaysTaken;
        private System.Windows.Forms.Label label7;
    }
}
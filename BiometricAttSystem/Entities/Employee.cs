﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiometricAttSystem.Entities
{
    public class Employee
    {
        public int EnrolmentId { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeFName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string EmployeeLName { get; set; }
        public string AadharNo { get; set; }
        public Byte[] empPic { get; set; }
        public Byte[] f1t1 { get; set; }
        public Byte[] f1t2 { get; set; }
        public Byte[] f2t1 { get; set; }
        public Byte[] f2t2 { get; set; }
        public UInt32 f1t5 { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiometricAttSystem.Entities
{
   public class Attendance
    {
        public string EmployeeEnrolmentId { get; set; }
        public string DateIn { get; set; }
        public string TimeIn { get; set; }
        public string DateOut { get; set; }
        public string TimeOut { get; set; }
        public int DaysAttended { get; set; }
    }
}

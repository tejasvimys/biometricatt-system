﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiometricAttSystem.Entities
{
   public class LeaveType:Employee
    {
        public int LeaveTypeId { get; set; }
        public string TypeOfLeave { get; set; }
        public string Abbreviation { get; set; }
        public int NoofDays { get; set; }
    }

    public class Leave: LeaveType
    {
        public int empid { get; set; }
        public string leavefromdate { get; set; }
        public string leaveToDate { get; set; }
    }
}

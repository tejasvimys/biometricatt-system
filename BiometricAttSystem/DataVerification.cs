﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiometricAttSystem
{
    public partial class DataVerification : Form
    {
        public DataVerification(string firstname, string lastname, string timein, string datein,Byte[] photo)
        {
            InitializeComponent();
            lblDateIn.Text = datein;
            lblTimeIn.Text = timein;
            lblFirstName.Text = firstname;
            lblLastName.Text = lastname;
            pbEmployeePhoto.Image = byteArrayToImage(photo);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Seeds_Billing_App_Utilities;
using SecuGen.FDxSDKPro.Windows;
using BiometricAttSystem.Entities;
using BiometricAttSystem.DataLayer;

namespace BiometricAttSystem
{
    public partial class EnterLeave : Form
    {
        public EnterLeave()
        {
            InitializeComponent();
        }


        private void EnrolUser_Load(object sender, EventArgs e)
        {
            GetEmployee();
            GetLeaveType();

        }

        //save leave details
        private void btnef1_Click_1(object sender, EventArgs e)
        {
           try
            {
                var leave = new DataLayer.Leave();
                var objLeave = new Entities.Leave();

                objLeave.empid = (Convert.ToInt32(cmbEmp.SelectedValue));
                objLeave.leavefromdate = Convert.ToString(dtFrm.Value.ToShortDateString());
                objLeave.leaveToDate = Convert.ToString(dtTo.Value.ToShortDateString());
                objLeave.TypeOfLeave = Convert.ToString(cmbLeaveType.SelectedValue.ToString());
                objLeave.NoofDays = Convert.ToInt32(txtNoofDaysTaken.Text.Trim());

                leave.EnterLeave(objLeave);
                MessageBox.Show("Leave Added Successfully");
            }
            catch
            {
                MessageBox.Show("Error found! Please contact Administrator");
            }
        }
        private void GetEmployee()
        {
            try
            {
                var getEmployee = new DataLayer.Leave();
                var emp = getEmployee.GetEmployeeList();

                var defBank = new Employee()
                {
                    EnrolmentId = 0,
                    EmployeeFName = "-Select Employee Details-"
                };
                emp.Insert(0, defBank);
                cmbEmp.DataSource = new BindingSource(emp, null);
                cmbEmp.DisplayMember = "EmployeeFName";
                cmbEmp.ValueMember = "EnrolmentId";
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw ex;
            }
        }

        private void GetLeaveType()
        {
            try
            {
                var getEmployee = new DataLayer.Leave();
                var emp = getEmployee.GetLeaveType();

                var defBank = new LeaveType()
                {
                    LeaveTypeId = 0,
                    TypeOfLeave = "-Select Leave Type-"
                };
                emp.Insert(0, defBank);
                cmbLeaveType.DataSource = new BindingSource(emp, null);
                cmbLeaveType.ValueMember = "TypeOfLeave";
                cmbLeaveType.DisplayMember = "TypeOfLeave";
               
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw ex;
            }
        }

        private void GetTotalLeaves(int empId, string leaveType)
        {
            try
            {
                var getEmployee = new DataLayer.Leave();
                var emp = getEmployee.GetNoofLeavesAvailed(empId, leaveType);
                lblNoOfDays.Text = emp.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                MessageBox.Show("Error in application! Contact Product Owner!");
            }
        }

        private int GetLeavesByLeaveType(string leaveType)
        {
            try
            {
                var getEmployee = new DataLayer.Leave();
                var emp = getEmployee.GetNoOfLeavesByLeaveType( leaveType);
                return emp;
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                MessageBox.Show("Error in application! Contact Product Owner!");
            }
            return 0;
        }

        private void cmbLeaveType_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        public void CalculateLeaves(DateTime dtIn, DateTime dtOut)
        {
            try
            {
                lblNoOfDays.Text = GetNumberOfWorkingDaysJeroen(dtIn, dtOut).ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.LogFileWrite(ex.ToString());
                throw ex;
            }
        }

        private static int GetNumberOfWorkingDaysJeroen(DateTime start, DateTime stop)
        {
            int days = 0;
            while (start <= stop)
            {
                if (start.DayOfWeek != DayOfWeek.Sunday)
                {
                    ++days;
                }
                start = start.AddDays(1);
            }
            return days;
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            var datein = dtFrm.Value;
            var dateout = dtTo.Value;
            CalculateLeaves(datein, dateout);
        }

        private void cmbLeaveType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var empid = Convert.ToInt32(cmbEmp.SelectedValue);
            var leaveType = cmbLeaveType.SelectedValue.ToString();
            if (leaveType != string.Empty || leaveType != "BiometricAttSystem.Entities.LeaveType")
            {
                GetTotalLeaves(empid, leaveType);
                var totalLeaves = GetLeavesByLeaveType(leaveType);
                var totalLeavesAvailed = lblNoOfDays.Text.Trim() == string.Empty ? 0 : Convert.ToInt32(lblNoOfDays.Text.Trim());
                var remLeaves = Convert.ToInt32(totalLeaves) - totalLeavesAvailed;
                txtnoofdaysremain.Text = remLeaves.ToString();
            }
        }

        private void txtNoofDaysTaken_TextChanged(object sender, EventArgs e)
        {
            var totalleavesapplied = txtNoofDaysTaken.Text.Trim();
            //var totalLeaves = 
        }

        private void txtNoofDaysTaken_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || (e.KeyChar == (char)Keys.Back)))
                e.Handled = true;
        }
    }
}

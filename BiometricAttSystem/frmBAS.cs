﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiometricAttSystem
{
    public partial class frmBAS : Form
    {
        public frmBAS()
        {
            InitializeComponent();
        }

        private void tspEnrollAttendance_Click(object sender, EventArgs e)
        {
          
        }

        private void eNROLLUSERFORATTENDANCEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dataverification = new EnrolUser();
            dataverification.ShowDialog(this);
            dataverification.Dispose();
        }

        private void eNTERLEAVEDETAILSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dataverification = new EnterLeave();
            dataverification.ShowDialog(this);
            dataverification.Dispose();
        }

        private void aTTENDANCEREPORTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dataverification = new AttendanceReport();
            dataverification.ShowDialog(this);
            dataverification.Dispose();
        }

        private void nOOFDAYSATTENDEDREPORTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dataverification = new OverallAttendanceRpt();
            dataverification.ShowDialog(this);
            dataverification.Dispose();
        }

        private void gETLEAVEREPORTSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dataverification = new LeaveReport();
            dataverification.ShowDialog(this);
            dataverification.Dispose();
        }
    }
}
